using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Turret : MonoBehaviour
{
    [SerializeField] private float _delayBeforeShooting = 0f;
    [SerializeField] private float _delayAfterShooting = 3f;
    [SerializeField] private GameObject _bulletPrefab;
    [SerializeField] private Transform _bulletSpawn;
    
    void Start()
    {
        InvokeRepeating("Fire", _delayBeforeShooting, _delayAfterShooting);
    }

    private void Fire()
    {
        Instantiate(_bulletPrefab, _bulletSpawn.position, transform.rotation);
    }
    
}
