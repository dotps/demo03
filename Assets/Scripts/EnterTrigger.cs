using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class EnterTrigger : MonoBehaviour
{
    [SerializeField] private UnityEvent<GameObject> _event;

    private void OnTriggerEnter(Collider other)
    {
        _event?.Invoke(other.gameObject);
    }
    
}
