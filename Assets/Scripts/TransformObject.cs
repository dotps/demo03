using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TransformObject : MonoBehaviour
{
    [Header("Rotation")]
    [SerializeField] private bool _isRotationAwake;
    [SerializeField] private Vector3 _rotationDirection = Vector3.up;
    [SerializeField] private float _rotationSpeed = 100f;
   
    [Header("Movement")]
    [SerializeField] private bool _isMoveAwake;
    [SerializeField] private Vector3 _moveDirection = Vector3.forward;
    [SerializeField] private float _moveSpeed = 100f;
    
    void Update()
    {
        Rotate();
        Move();
    }

    public void Rotate()
    {
        if (!_isRotationAwake) return;
        transform.Rotate(_rotationSpeed * Time.deltaTime * _rotationDirection);
    }

    public void Move()
    {
        if (!_isMoveAwake) return;
        transform.Translate(_moveSpeed * Time.deltaTime * _moveDirection);
    }

}
