using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spike : MonoBehaviour
{
    [SerializeField] private Transform _spike;
    [SerializeField] private float _moveTime = 1f;
    [SerializeField] private float _standByTime = 2f;
    [SerializeField] private float _startDelay = 0f;
    [SerializeField] private float _spikeOpenOffsetY = 0.5f;

    [SerializeField] [Range(-1,1)] private int _direction = 1;
    
    private void Start()
    {
        _spike.position -= new Vector3(0, _spikeOpenOffsetY, 0);
        Invoke("StartAnimation", _startDelay);
    }

    private void StartAnimation()
    {
        StartCoroutine(SpikeAnimation());
    }

    private IEnumerator SpikeAnimation()
    {
        float time = 0f;
        Vector3 currentPosition = _spike.position;
        while (time < _moveTime)
        {
            time += Time.deltaTime;
            Vector3 position = Vector3.Lerp(currentPosition, currentPosition + new Vector3(0, _spikeOpenOffsetY * _direction, 0), time);
            _spike.position = position;
            yield return null;
        }
        yield return new WaitForSeconds(_standByTime);
        
        _direction *= -1;
        StartCoroutine(SpikeAnimation());
    }
}
