﻿using UnityEngine;

namespace Game
{
public class Key : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        if (other.TryGetComponent(out Player player))
        {
            Interaction(player);
            Destroy(gameObject);
        }
    }
    
    protected virtual void Interaction(Player player)
    {
        player.PickUpKey();
    }
}
}