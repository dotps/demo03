using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class EnterCollision : MonoBehaviour
{

    [SerializeField] private UnityEvent _event;

    private void OnCollisionEnter(Collision collision)
    {
        _event?.Invoke();
    }
}
