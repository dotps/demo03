using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyObject : MonoBehaviour
{
    [SerializeField] private GameObject _targetToDestroy;
    
    public void Destroy()
    {
        if (_targetToDestroy == null) _targetToDestroy = gameObject;
        Destroy(_targetToDestroy);
    }
    
}
