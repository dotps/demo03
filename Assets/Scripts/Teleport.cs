using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Game
{
    public class Teleport : Key
    {
        [SerializeField] private Transform _destination;
        
        protected override void Interaction(Player player)
        {
            player.SetPosition(_destination.position);
        }

    }

}
